# README #

### How do I get set up? ###

- Create a catkin workspace
- Move to the source directory (`cd ~/catkin_ws/src`)
- Clone this repo (`git clone ...`)
- You should have a `potoo` folder within `src`
- Build the project! (`cd ~/catkin_ws/src && catkin_make`)