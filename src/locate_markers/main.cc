#include <ros/ros.h>

#include <stdio.h>

int main(int argc, char* argv[]) {
  // Initialize ROS.
  ros::init(argc, argv, "locate_markers");

  while (ros::ok()) {
    ros::spinOnce();
  }
}
